module gitea.com/tango/xlog_bridge

go 1.12

require (
	gitea.com/lunny/tango v0.6.5
	gitea.com/lunny/xlog v0.0.0-20210305075605-5b8499deada8
)
