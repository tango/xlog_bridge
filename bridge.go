package logbridge

import (
	"strings"

	"gitea.com/lunny/tango"
	log "gitea.com/lunny/xlog"
)

// XLogBridge a logger bridge from Logger to gitea log
type XLogBridge struct {
	logger *log.Logger
}

// NewXLogger inits a bridge for gitea log
func NewXLogger(name string) tango.Logger {
	return &XLogBridge{
		logger: log.GetLogger(name),
	}
}

// Debug show debug log
func (l *XLogBridge) Debug(v ...interface{}) {
	l.logger.Debug(strings.Repeat("%v ", len(v)), v...)
}

// Debugf show debug log
func (l *XLogBridge) Debugf(format string, v ...interface{}) {
	l.logger.Debug(format, v...)
}

// Error show error log
func (l *XLogBridge) Error(v ...interface{}) {
	l.logger.Error(strings.Repeat("%v ", len(v)), v...)
}

// Errorf show error log
func (l *XLogBridge) Errorf(format string, v ...interface{}) {
	l.logger.Error(format, v...)
}

// Info show information level log
func (l *XLogBridge) Info(v ...interface{}) {
	l.logger.Info(strings.Repeat("%v ", len(v)), v...)
}

// Infof show information level log
func (l *XLogBridge) Infof(format string, v ...interface{}) {
	l.logger.Info(format, v...)
}

// Warn show warning log
func (l *XLogBridge) Warn(v ...interface{}) {
	l.logger.Warn(strings.Repeat("%v ", len(v)), v...)
}

// Warnf show warnning log
func (l *XLogBridge) Warnf(format string, v ...interface{}) {
	l.logger.Warn(format, v...)
}
